# Makefile -- to build a Debian Docker Machine
# Copyright (C) 2017  Olaf Meeuwissen
#
# License: GPL-3.0+

fullname ?= Moby\ Dock
username ?= mobydock
password ?= mobydock

https_proxy ?=
http_proxy  ?=
ntp_server  ?=

headless ?=

debian_builder = packer build $<
docker-machine: machine.json debian/preseed.cfg
	@echo "Running \`${debian_builder}\`"
	@(export username=${username}; \
	  export password=${password}; \
	  export https_proxy=${https_proxy} \
	  export http_proxy=${http_proxy}   \
	  export ntp_server=${ntp_server};  \
	  ${debian_builder})

debian/preseed.cfg: debian/preseed.cfg.in
	(umask 077; \
	sed -e "s|@_fullname_@|${fullname}|g" \
	    -e "s|@_username_@|${username}|g" \
	    -e "s|@_password_@|${password}|g" \
	    -e "s|@_http_proxy_@|${http_proxy}|g" \
	    -e "s|@_ntp_server_@|${ntp_server}|g" \
	    $< > $@ \
	|| (rm $@; exit 1))

clean:
	-rm debian/preseed.cfg
