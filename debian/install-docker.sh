#!/bin/sh -eux
# install-docker.sh -- from the upstream APT repository
# Copyright (C) 2017, 2019  Olaf Meeuwissen
#
# License: GPL-3.0+

DOCKER_KEY_ID=0EBFCD88

apt-get install --quiet --assume-yes \
        apt-transport-https \
        ca-certificates

name=$(awk '$1 ~ /^deb/ {print $3; exit}' /etc/apt/sources.list)
arch=$(dpkg --print-architecture)

wget -q -O - https://download.docker.com/linux/debian/gpg \
    | apt-key add -
test -n "$(apt-key fingerprint $DOCKER_KEY_ID 2>/dev/null)"
cat <<EOF > /etc/apt/sources.list.d/docker.list
deb [arch=$arch] https://download.docker.com/linux/debian $name stable
EOF

apt-get update  --quiet
apt-get install --quiet --assume-yes \
        docker-ce

adduser $SUDO_USER docker

if test -n "$https_proxy"; then
    confdir=/etc/systemd/system/docker.service.d
    mkdir -p $confdir
    cat <<EOF > $confdir/http-proxy.conf
[Service]
Environment="http_proxy=$https_proxy"
EOF
    systemctl daemon-reload
    systemctl restart docker
fi
